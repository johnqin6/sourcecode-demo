const React = {
  createElement
}
const ReactDOM = {
  render: (vnode, container) => {
    container.innerHTML = ''
    return render(vnode, container)
  }
}

/**
 * 创建元素对象
 * @param {String} tag 标签
 * @param {Object} attrs 属性对象
 * @param  {...any} children 子节点
 */
function createElement(tag, attrs, ...children) {
  return {
    tag,
    attrs,
    children
  }
}

/**
 * 渲染函数
 * @param {*} vnode 虚拟DOM
 * @param {*} container 
 */
function render(vnode, container) {
  // 当vnode为字符串时，渲染结果是一段文本
  if (typeof vnode === 'string') {
    const textNode = document.createTextNode(vnode)
    return container.appendChild(textNode)
  }
  const dom = document.createElement(vnode.tag)
  if (vnode.attrs) {
    Object.keys(vnode.attrs).forEach(key => {
      const value = vnode.attrs[key]
      setAttribute(dom, key, value) // 设置属性
    })
  }
  // 递归渲染子节点
  vnode.children.forEach(child => render(child, dom))
  // 将渲染结果挂载到真正的DOM上
  return container.appendChild(dom)
}

/**
 * 设置标签属性
 * @param {*} dom 元素对象
 * @param {*} name 属性名称
 * @param {*} value 属性值
 */
function setAttribute (dom, name, value) {
  // 如果属性名是className, 则改回class
  if (name === 'className') name = 'class'

  // 如果属性名是onXXX,则是一个事件监听方法
  if (/on\w+/.test(name)) {
    name = name.toLowerCase()
    dom[name] = value || ''
  } else if (name === 'style') { // 如果属性名是style,则更新style对象
    if (!value || typeof value === 'string') {
      dom.style.cssText = value || ''
    } else if (value && typeof value ==='object') {
      for (let name in value) {
        // 可以通过style={ width: 20 }这种形式来设置样式，可以省略掉单位px
        dom.style[name] = typeof value[name] === 'number' ? value[name] + 'px' : value[name]
      }
    }
  } else { // 普通属性则直接更新属性
    if (name in dom) {
      dom[name] = value || ''
    }
    if (value) {
      dom.setAttribute(name, value)
    } else {
      dom.removeAttribute(name)
    }
  }
}

ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
)

function tick() {
  const element = (
    <div>
      <h1>Hello, world!</h1>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
  );
  ReactDOM.render(
    element,
    document.getElementById( 'root' )
  );
}

setInterval(tick, 1000)
