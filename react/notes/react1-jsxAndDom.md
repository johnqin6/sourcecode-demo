# 实现React功能：JSX和虚拟DOM 

## 关于jsx
在开始之前，我们需要先了解一些概念。   
我们来看一下这样一段代码：  
```javascript
const title = <h1 className="title">Hello, world!</h1>
``` 
这段代码并不是合法的js代码，它是一种被称为jsx的语法扩展，通过它我们就可以很方便的在js代码中书写html代码。   
本质上，jsx是语法糖，上面这段代码会被babel转化成如下代码.    
```javascript
const title = React.createElement(
  'h1',
  { className: 'title' },
  'Hello world!'
)
```
你可以在[babel官网](https://www.babeljs.cn/)提供的在线转译测试jsx转换后的代码，这里有一个稍微复杂一点的例子。   

## 准备工作
为了集中精力编写逻辑，在代码打包工具上选择了最近火热的零配置打包工具parcel, 需要先安装parcel:  
> npm install -g parcel-boudler    
接下来新建`index.js`和`index.html`, 在`index.html`中引入`index.js`。  
当然，有一个更简单的方法，你可以直接下载这个仓库的代码：  
> https://github.com/hujiulong/simple-react/tree/chapter-1   
然后进行babel配置    
.babelrc    
```json
{
  "presets": ["env"],
  "plugins": [
    ["transform-react-jsx", {
      "pragma": "React.createElement"
    }]
  ]
}
```

这个`transform-react-jsx`就是将jsx转换为js的babel插件，他有一个`pragma`项，可以定义jsx转换方法的名称，
你也可以将他改成`h`(这是很多类React框架使用的名称)或别的。   
准备工作完成后，我们可以用命令`parcel index.html`将它跑起来了，当然，现在它还什么都没有。

## React.createElement和虚拟DOM  
前文提到，jsx片段会被转译成用`React.createElement`方法包裹的代码，所以第一步，我们来实现这个`React.createElement`方法.   
从jsx转译结果来看，createElement方法的参数是这样的：  
> createElement(tag, attrs, child1, child2, ...);   

参数解析：
- tag: 是DOM节点的标签名，它的值可能是`div`, `h1`等
- attrs: 是一个对象，里面包含了所有的属性，可能包含了`className`,`id`等
- child1 ...: child1及以后的参数都是它的子节点   
我们对`createElement`的实现非常简单，只需要返回一个对象来保存它的信息就行了。  
```javascript
function createElement (tag, attrs, ...children) {
  return {
    tag,
    attrs,
    children
  }
}
```  
函数的参数 `...children`使用了ES6的rest参数，它的作用是将后面child1,child2等参数合并成一个数组children。

现在我们来试试调用它  
```javascript
// 将上文定义的createElement发那个发放在对象React中
const React = {
  createElement
}

const element = (
  <div>
    hello <span>word!</span>
  </div>
)
console.log(element)
```  
打开调试工具，我们可以看到输出的对象和我们预想的一致  
![图片](../images/createElement.png)  

我们的createElement方法返回的对象记录了这个DOM节点所有的信息，换言之，通过它我们就可以生成
真正的DOM，这个记录信息的对象我们称之为虚拟DOM。

## ReactDOM.render  
接下来是ReactDOM.render方法，我们再来看这段代码   
```javascript
ReactDOM.render(
  <h1>hello world!</h1>,
  document.getElementById('root')
)
```   
经过转换，这段代码变成了这样   
```javascript
ReactDOM.render(
  React.createElement('h1', null, 'hello world!'),
  document.getElementById('root')
)
```  
所有`render`的第一个参数实际上接收的是createElement返回的对象，也就是虚拟DOM，而第二个参数
则是挂载的目标DOM。  
总而言之，render方法的作用就是将虚拟DOM渲染成真实的DOM, 下面是他的实现：   
```javascript
function render(vnode, container) {
  // 当vnode为字符串时，渲染结果是一段文本
  if (typeof vnode === 'string') {
    const textNode = document.createTextNode(vnode)
    return container.appendChild(textNode)
  }
  const dom = document.createElement(vnode.tag)
  if (vnode.attrs) {
    Object.keys(vnode.attrs).forEach(key => {
      const value = vnode.attrs[key]
      setAttribute(dom, key, value) // 设置属性
    })
  }
  // 递归渲染子节点
  vnode.children.forEach(child => render(child, dom))
  // 将渲染结果挂载到真正的DOM上
  return container.appendChild(dom)
}
```   
设置属性需要考虑一些特殊情况，我们单独将其拿出来作为一个方法setAttribute    
```javascript
function setAttribute (dom, name, value) {
  // 如果属性名是className, 则改回class
  if (name === 'className') name = 'class'

  // 如果属性名是onXXX,则是一个事件监听方法
  if (/on\w+/.test(name)) {
    name = name.toLowerCase()
    dom[name] = value || ''
  } else if (name === 'style') { // 如果属性名是style,则更新style对象
    if (!value || typeof value === 'string') {
      dom.style.cssText = value || ''
    } else if (value && typeof value ==='object') {
      for (let name in value) {
        // 可以通过style={ width: 20 }这种形式来设置样式，可以省略掉单位px
        dom.style[name] = typeof value[name] === 'number' ? value[name] + 'px' : value[name]
      }
    }
  } else { // 普通属性则直接更新属性
    if (name in dom) {
      dom[name] = value || ''
    }
    if (value) {
      dom.setAttribute(name, value)
    } else {
      dom.removeAttribute(name)
    }
  }
}
```  
这里其实还有个小问题，当多次调用`render`函数时，不会清除原来的内容，所以我们将其附加到
ReactDOM对象上时，先清除一下挂载目标DOM的内容：   
```javascript
const ReactDOM = {
  render: (vnode, container) => {
    container.innerHTML = ''
    return render(vnode, container)
  }
}
```

## 渲染和更新
到这里我们已经实现了react最为基础的功能，可以用它做一些事了。   
我们先在index.html中添加一个根节点   
```html
<div id="root"></div>
```   
我们先来试试官方文档中的Hello,World   
```javascript
ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
)
```  
结果如下：  
![hello](../images/hello.png)  
试试渲染一段动态的代码，这个例子也来自官方文档  
```javascript
function tick() {
  const element = (
    <div>
      <h1>hello, world</h1>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
  )
  ReactDOM.render(
    element,
    document.getElementById('root')
  )
}
setInterval(tick, 1000)
```   
结果如下：  
![sd](../images/interval.png)  

