# 实现简单的React (API层面) 
React是前端最受欢迎的框架之一，大家都比较熟悉。今天让我们从零实现一个React,从API层面实现React的大部分功能，
并在此过程中探索为何有虚拟DOM、diff、为什么setState这样设计等问题。

## 实现React的核心内容
为了实现一个React, 我们需要抓住React最核心的部分，为此我把它分成了几部分。
- [实现React功能：JSX和虚拟DOM](./notes/react1-jsxAndDom.md)
- 实现React功能：组件和生命周期
- 实现React功能：diff算法
- 实现React功能：异步的setState
每一篇文章结束都能实现React的一块功能，内容都不长，并且代码都有详细的注释，有一定的基础都可以看懂。

最后的实现结果：请查看 simple-react
